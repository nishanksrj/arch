// Initialize the platform object:
var platform = new H.service.Platform({
  'app_id': 'myawyUf5nPfe914BSGfp',
  'app_code': 'F2A2ZPi1ETd6lGpeBLmJqA',
  useHTTPS: true
});

var pixelRatio = window.devicePixelRatio || 1;

// Obtain the default map types from the platform object
var defaultLayers = platform.createDefaultLayers({
  tilesize: pixelRatio === 1?  256:512,
  ppi: pixelRatio === 1? undefined: 320,
});


var pos;
var marker;
var map;
var isShapeComplete = false;
var linestring = new H.geo.LineString();
var count = 0;
var json = '{ "coord_json": {"coordinates":[';
var b = true;
var mapLoaded = true;

function initializeMap(){

  map = new H.Map(
    document.getElementById('mapContainer'),
    defaultLayers.normal.map,
    {
      zoom: 18,
      center: pos,
      pixelRatio: pixelRatio
    }
  );

  map.setBaseLayer(defaultLayers.satellite.xbase);

  var mapEvents = new H.mapevents.MapEvents(map);
  var behavior = new H.mapevents.Behavior(mapEvents);
  var ui = H.ui.UI.createDefault(map, defaultLayers);
  ui.removeControl('mapsettings');

  marker = new H.map.Marker(pos);
  map.addObject(marker);

  // map.addEventListener('tap', handleMapClick);

  $("#addLocationBtn").click(handleBtnClick);
  $("#currentLocationBtn").click(setCurrentLocation);

  // function handleMapClick(event){
  //   var coord = map.screenToGeo(event.currentPointer.viewportX, event.currentPointer.viewportY);
  //   addPoint(new H.geo.Point(coord.lat, coord.lng, 100));
  //   if(isShapeComplete===true){
  //     $("#addLocationBtn").off('click');
  //     map.removeEventListener('tap', handleMapClick, false);
  //   }
  // }

  function handleBtnClick(){
    if(linestring.getPointCount()!=0){
      var pnt = linestring.extractPoint(linestring.getPointCount()-1);
      if(pos.lat==pnt.lat && pos.lng==pnt.lng){
        window.alert("This point is already marked.\nMove to new point.");
        return;
      }
    }
    addPoint(new H.geo.Point(pos.lat, pos.lng, 100));
    if(isShapeComplete===true){
      $("#addLocationBtn").off('click');
      // map.removeEventListener('tap', handleMapClick, false);
    }
  }
  $('.se-pre-con').css('display','none');
  $('#addLocationBtn').css('visibility','visible');
  $('#currentLocationBtn').css('visibility','visible');
}

function addPoint_Json(geoPoint){
  count = count + 1
  json = json + '{ "lat": "' + geoPoint.lat + '", "lng" : "' + geoPoint.lng + '"}';
}

function addPoint(geoPoint){
  if(linestring.getPointCount()===0){
    addCircleFlag(geoPoint);
    linestring.pushPoint(geoPoint);
    addPoint_Json(geoPoint);
  }
  else if(linestring.getPointCount()===1){
    addCircleFlag(geoPoint);
    linestring.pushPoint(geoPoint);
    polyline = new H.map.Polyline(linestring, { style: {strokeColor: 'red',lineWidth : 4}});
    map.addObject(polyline);
    json = json + ',';
    addPoint_Json(geoPoint);
  }
  else if(linestring.getPointCount()>1){
    if(geoPoint.distance(linestring.extractPoint(0))<=5){
      geoPoint = linestring.extractPoint(0);
      isShapeComplete = true;

      var newGeoPolygon = new H.geo.Polygon(linestring);
      var newMapPolygon = new H.map.Polygon(newGeoPolygon);
      map.addObject(newMapPolygon);

      var rect = getExtendedRect(newMapPolygon.getBounds());
      var topLeft = rect.getTopLeft();
      var bottomRight = rect.getBottomRight();

      json = json + '], "count": "' + count.toString() + '"},';
      json = json +  '"geo_json":{"type":"Feature","properties":{},"geometry":{"type": "Polygon","coordinates":[[[' + topLeft.lng +',' + topLeft.lat +'],[' + bottomRight.lng + ',' + topLeft.lat + '],[' + bottomRight.lng + ',' + bottomRight.lat + '],[' + topLeft.lng + ',' + bottomRight.lat + '],[' + topLeft.lng + ',' + topLeft.lat + ']]]}}'
      json = json + '}';
      $('#coord_json').val(json);
    }
    else{
      json = json + ',';
      addPoint_Json(geoPoint);
      addCircleFlag(geoPoint);
    }
    linestring.pushPoint(geoPoint);
    polyline.setGeometry(linestring);
  }
  console.log(linestring.getPointCount());
}

function getExtendedRect(rect){
  var topLeft = rect.getTopLeft();
  var bottomRight = rect.getBottomRight();
  var topRight = new H.geo.Point(bottomRight.lat, topLeft.lng);
  var bottomLeft = new H.geo.Point(topLeft.lat, bottomRight.lng);
  var l = topLeft.distance(topRight);
  var w = bottomRight.distance(topRight);
  var area = l*w;
  if(area<12000){
    s_r = Math.sqrt(12000/area);
    var n_l = s_r*l;
    var n_w = s_r*w;
    var d1 = (n_l-l)/2;
    var d2 = (n_w-w)/2;
    topLeft = topLeft.walk(-90,d2);
    topLeft = topLeft.walk(0,d1);
    bottomRight = bottomRight.walk(90,d2);
    bottomRight = bottomRight.walk(180,d1);
    rect = new H.geo.Rect(topLeft.lat,topLeft.lng,bottomRight.lat,bottomRight.lng);
  }
  return rect;
}

function addCircleFlag(coord){
  var marker = new H.map.Circle(
    { lat: coord.lat, lng: coord.lng},
    3,
    {
      style:{
        strokeColor: 'white',
        lineWidth: 2,
        fillColor: 'black'
      }
    }
  );
  map.addObject(marker);
}

function setCurrentLocation(){
  marker.setPosition(pos);
  map.setCenter(pos);
}


function updateLocation(position){
  pos = {lat: position.coords.latitude, lng: position.coords.longitude};
  if(b){
    initializeMap();
    b = false;
  }
  else
    setCurrentLocation();
}

function init(){
  if(navigator.geolocation){
    watchOptions = {enableHighAccuracy:true, timeout:60000, maximumAge:0,distanceFilter:0};
    navigator.geolocation.watchPosition(updateLocation, showError,watchOptions);
  }
  else{
    alert("Geolocation is not supported by this browser.");
  }
}

function showError(error){
  switch(error.code){
    case error.PERMISSION_DENIED:
    alert("Permission denied.");
    break;
    case error.PERMISSION_UNAVAILABLE:
    alert("Location not available");
    break;
    case error.TIMEOUT:
    alert("Permission request timed out.\n Refresh the page.");
    break;
    case error.UNKNOWN_ERROR:
    alert("Unknown error occurred.");
    break;
  }
}


$("#contact-submit").click(function(){
  var form = document.querySelector('form');
  if(!form.checkValidity())
    form.reportValidity();
  else{
    $("#map_modal").modal('show');
    if(mapLoaded){
      window.setTimeout(init, 200);
      mapLoaded = false;
    }
  }
});

$('form').submit(function(){
  if($('#coord_json').val()==""){
    $('#map_modal').modal('show');
    if(mapLoaded){
      window.setTimeout(init, 200);
      mapLoaded = false;
    }
    return false;
  }
  else
    return true;
});

$(window).on('load',function(){
  $('#myModal').modal('show');
});
