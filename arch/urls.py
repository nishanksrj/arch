"""arch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import tag, app, api

urlpatterns = [
    path('auth/', include('django.contrib.auth.urls')),
    path('tag/', include('tag.urls')),
    path('api/v1/',include('api.urls')),
    path('admin/', admin.site.urls),
    path('', include('app.urls'))
    #path('admin/', admin.site.urls),
    #path('/geo', include('app.urls'))
]

admin.site.site_title = "Agrostar"
admin.site.index_title = "Arch"
admin.site.site_header = "Agrostar"

handler403 = 'app.views.handler403'
handler404 = 'app.views.handler404'
handler500 = 'app.views.handler500'
