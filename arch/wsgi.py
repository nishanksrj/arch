"""
WSGI config for arch project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application


# wsgi.py is required for apache2 server and heroku
# below line defines the default module for django settings file (apache2)
# heroku have a environment variable DJANGO_SETTINGS_MODULE which overwrites it
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'arch.settings.production')

application = get_wsgi_application()
