from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from .serializers import *

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from app.models import *

import urllib, json

# import logging
#
# logging.basicConfig(
#     level = logging.INFO,
#     format = '%(asctime)s %(levelname)s %(message)s',
#     filename = '/tmp/data.log',)

class FarmerViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    queryset = Farmer.objects.all()
    serializer_class = FarmerSerializer

    def retrieve(self, request, pk = None):
        try:
            object = Farmer.objects.get(Q(mobile1 = pk)|Q(mobile2 = pk)|Q(mobile3 = pk))
            serializer = FarmerSerializer(object)
            return Response(serializer.data)
        except ObjectDoesNotExist:
            try:
                url = "https://crm.agrostar.in/crmservice/v2/farmerdetailsopen/?searchType=mobile&searchTerm=" + str(pk)
                response = urllib.request.urlopen(url)
                json_data = json.loads(response.read().decode('utf-8'))
                farmerData = json_data['responseData']['farmers']
                farmer = {}
                if farmerData:
                    farmerData = farmerData[0]
                    address = farmerData['profileAddress']
                    farmer['cid'] = farmerData['farmerId']
                    farmer['name'] = farmerData['firstName'] + ' ' + farmerData['middleName'] + ' '  + farmerData['lastName']
                    farmer['address'] = address['village'] + ', ' + address['district'] + ', ' + address['state']
                    farmer['mobile1'] = pk
                    farmer['plots'] = []
                    return Response(farmer)
                else:
                    return Response({"detail": "User does not exist."})
            except Exception as e:
                print(e)
                return Response({"detail": "Some error occurred. Please contact administration."})



class PlotViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    queryset = Plot.objects.all()
    serializer_class = PlotSerializer

    def perform_create(self, serializer):
        farmer_cid = self.request.data.get("farmer")
        farmer = None
        try:
            farmer = Farmer.objects.get(cid = farmer_cid)
        except:
            try:
                url = "https://crm.agrostar.in/crmservice/v2/farmerdetailsopen/?searchType=farmerId&searchTerm=" + str(farmer_cid)
                response = urllib.request.urlopen(url)
                json_data = json.loads(response.read().decode('utf-8'))
                farmerData = json_data['responseData']['farmers'][0]
                address = farmerData['profileAddress']
                farmer = Farmer(cid = farmerData['farmerId'])
                farmer.mobile1 = farmerData['mobile1']
                farmer.mobile2 = farmerData['mobile2']
                farmer.mobile3 = farmerData['mobile3']
                farmer.firstName = farmerData['firstName']
                farmer.middleName = farmerData['middleName']
                farmer.lastName = farmerData['lastName']
                farmer.village = address['village']
                farmer.taluka = address['taluka']
                farmer.district = address['district']
                farmer.state = address['state']
                farmer.pincode = address['pinCode']
                farmer.save(force_insert=True)
            except e as Exception:
                print(e)
                return
        serializer.save(farmer = Farmer.objects.get(cid = farmer_cid))
