from django.contrib.auth.models import User, Group
from app.models import *
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')



class PlotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plot
        fields = ('id','plot_name','coord_json')

class FarmerPlotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plot
        fields = ('plot_name',)

class FarmerSerializer(serializers.ModelSerializer):
    plots = FarmerPlotSerializer(read_only = True, many = True)
    name = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()

    def get_name(self, obj):
        name = obj.firstName
        if obj.middleName:
            name = name + " " + obj.middleName
        if obj.lastName:
            name = name + " " + obj.lastName
        return name

    def get_address(self, obj):
        return obj.village + ", " + obj.district + ", " + obj.state

    class Meta:
        model = Farmer
        fields = ('cid', 'name', 'address', 'mobile1' , 'plots')
