# Precision Agriculture

Precision Agriculture is a site specific crop management technique. It defines a decision support system with the goal of optimizing input efforts and preserving resources.

## Getting Started

This dashboard provides an interface to our decision support system and database management system. It is divided into three main apps
  - Dashboard
  - Geo-tag App
  - Dashboard API


### Dashboard

The Dashboard has five modules providing interface to mathematical and machine-learning models-
  - Nutrition Recomendation
  - Irrigation Requirement
  - Disease Prediction
  - Disease Detection via Image Classification
 

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Django 2.2.1](https://www.djangoproject.com/) - The web framework used
* [MySQL](https://www.mysql.com/) - Database used


## Resources

* [API World Weather Online](https://www.worldweatheronline.com/) - Weather API for weather forecast and backcast.
* [Here Map API](https://developer.here.com/documentation/maps/topics/quick-start.html) - Map API
* [Agromonitoring API](https://agromonitoring.com/) - API for NDVI Images and statistics


## Authors

* **Nishank Mishra**  - [nishanksrj](https://github.com/nishanksrj)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

