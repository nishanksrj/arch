from django import forms
from django.core.validators import RegexValidator

from .models import *


crops = Crop.objects.all().order_by('-name')
CROP_CHOICES = []
SEGMENT_CHOICES = []
for crop in crops:
    CROP_CHOICES.append((crop.name, crop.name))
    # segments = Segment.objects.filter(crop=crop)
    # for segment in segments:
    #     SEGMENT_CHOICES[crop.name].append((segment.name, segment.name))


class GeoDataForm(forms.Form):
    phone_regex = RegexValidator(regex=r'^\d{10}$', message="Phone number must be of 10 digits.")
    phone_no = forms.CharField(validators = [phone_regex],min_length=10, max_length = 10)
    coord = forms.CharField(max_length = 1024)


class DataFileForm(forms.Form):
    datafile =  forms.FileField(label = 'Select Soil Report', help_text = 'max. 42 MB.')

class ImageFileForm(forms.Form):
    datafile =  forms.FileField(label = 'Select Leaf Image', help_text = 'max. 42 MB.')

class ScheduleForm1(forms.Form):

    IRRIGATION_CHOICES = (('Drip','Drip'),('Flooding', 'Flooding'),('Micro-jet','Micro-jet'),('Non Irrigated','Non Irrigated'),('Pivot', 'Pivot'),('Sprinkler','Sprinkler'),)

    crop = forms.ChoiceField(choices = CROP_CHOICES)
    irrigation = forms.ChoiceField(choices = IRRIGATION_CHOICES)
    expected_yield = forms.FloatField()
    sdate = forms.DateTimeField(input_formats=['%d-%m-%Y',])

class ScheduleForm2(forms.Form):

    def __init__(self, crop, *args, **kwargs):
        super(ScheduleForm2, self).__init__(*args,**kwargs)
        self.SEGMENT_CHOICES = SEGMENT_CHOICES[crop]
        self.fields['segment'] = forms.ChoiceField(chohices = self.SEGMENT_CHOICES)
