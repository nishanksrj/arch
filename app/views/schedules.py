from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from ..forms import *
from ..models import *

from .nutrition import *
from .Pennman_Montieth import *
from .prediction import *

import requests, logging, datetime, pandas

logger = logging.getLogger('django_log')


@login_required
def schedules_table(request):
    """
    display table containing all schedules ordered in alphabetical orderr of crop name

    Args: HttpRequest

    Returns: HttpResponse
    """
    context = {}
    # query to get all schedules in sorted order
    # and set schedules in context as schedule parameter
    context['schedules'] = Schedule.objects.all().order_by('-crop__name')
    return render(request, 'app/schedules_table.html', context)


@login_required
def schedule(request, id):
    """
    Display an active schedule's all properties

    Args: HttpRequest

    Returns: HttpResponse
    """
    context = {}
    # GET request display the existing schedule for the plot whose id is given as argument
    # POST request create a new schedule for the plot whose id is given as argument
    if request.method=="POST":
        form = ScheduleForm(request.POST)
        context['form'] = form
        # validate the form
        if form.is_valid():
            form = form.cleaned_data
            try:
                # create a schedule for the plot with given id if schedule doesn't exist
                plot = Plot.objects.get(id=id)
                exist_schedule = Schedule.objects.filter(plot = plot)
                if exist_schedule:
                    messages.error(request, "A schedule already exists for this plot.")
                else:
                    s = Schedule(plot = plot)
                    s.crop = Crop.objects.get(name = form['crop'])
                    s.sdate = form['sdate']
                    s.irrigation = form['irrigation']
                    s.e_yield = form['expected_yield']
                    s.save()
                    messages.success(request, "Schedule created successfully.")
            except Exception as e:
                logger.error(str(e))
                messages.error(request, "Some error occurred.")
        else:
            logger.error(form.errors)
            messages.error(request, "Invalid data.")
    else:
        try:
            # display the schedule for the plot whose id was given as parameter
            plot = Plot.objects.get(id = id)
            try:
                # extract lat,lng of any one coordinate of the farm
                context['schedule'] = Schedule.objects.get(plot = plot)
                point = json.loads(context['schedule'].plot.coord_json)['coordinates'][0]
                context['lat'] = point['lat']
                context['lng'] = point['lng']
                try:
                    # get the day vs. stage data from popdev api (to be incorporated in the system as plant stages)
                    url = "https://popdev.agrostar.in/popservice/segment/?isActive=true&cropId="+ context['schedule'].crop.uuid
                    response = requests.get(url)
                    js = response.json()
                    segment_list = js['responseData']['segmentList']
                    for segment in segment_list:
                        if segment['noOfSchedules']>0:
                            segment_id = segment['uuid']
                            response = requests.get("https://popdev.agrostar.in/popservice/popschedule/?segmentId="+segment_id)
                            js = response.json()
                            schedule_id = js['responseData']['POPScheduleList'][0]['uid']
                            response = requests.get("https://popdev.agrostar.in/popservice/popstage/?popScheduleId="+schedule_id)
                            js = response.json()
                            context['stages'] = []
                            for stage in js['responseData']['popStageList']:
                                context['stages'].append([stage['startDay'], stage['endDay'], stage['popStageName']])
                            break
                except Exception as e:
                    logger.error(str(e))
                return render(request, 'app/schedule_profile.html', context)
            except ObjectDoesNotExist:
                # if no schedule exists for the plot then show a form to create schedule
                context['plot'] = plot
                context['crops'] = Crop.objects.all()
                return render(request, 'app/schedule_form.html', context)
            except Exception as e:
                logger.error(str(e))
        except Exception as e:
            logger.error(str(e))
            messages.error(request, "Some error occurred.")
    return redirect('schedules_table')

@login_required
def irrigation_schedule(request):
    """
    Return Irrigation Schedule (irrigation recommended vs. days after sowing) in json format

    Args: HttpRequest

    Returns: FileResponse(csv)/ HttpResponse(Json)
    """
    context = {}
    # get the schedule id from GET request
    id = request.GET.get("id")
    if id:
        try:
            schedule = Schedule.objects.get(id=id)
            # get any one coordinate of plot
            point = json.loads(schedule.plot.coord_json)['coordinates'][0]
            irrig, irrig_hat, days = run_for_cycle(point['lat'], point['lng'], schedule.crop.name)
            context['days'] = days
            context['raw'] = irrig
            context['smoothened'] = irrig_hat
            context['status'] = True
        except Exception as e:
            context['message'] = str(e)
            context['status'] = False
    else:
        context['status'] = False
        context['message'] = "Id parameter not specified."

    # if csv format is required then modify the response and show it in csv format
    if request.GET.get("format") and request.GET.get("format")=="csv":
        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=schedule.csv'
        if not context['status']:
            # if something is wrong, add error message
            context['message'] = [context['message']]
        del context['status']
        df = pandas.DataFrame(context)
        df.to_csv(response, index = False)
        return response
    else:
        return HttpResponse(json.dumps(context), content_type="application/json")


@login_required
def dp(request):
    """
    Return Disease prediction for next 7 days

    Args: HttpRequest

    Returns: FileResponse(csv) /HttpResponse(Json)
    """
    context = {}
    id = request.GET.get("id")
    if id:
        try:
            # get the schedule from db whose id is given
            sch = Schedule.objects.get(id=id)
            # get any one coordinate from plot
            point = json.loads(sch.plot.coord_json)['coordinates'][0]
            # get the actual prediction lists from get_prediction function
            response = get_prediction(sch.crop.name, point['lat'], point['lng'], 'r')
            if response['status']:
                # if everything is fine, then add it in response
                days = response['prob']
                context['prediction'] = zip(response['map'], list(map(list, zip(*days))))
                context['map'] = response['map']
                context['status'] = True
            else:
                # if some error occurred, then show error message
                context['status'] = False
                context['message'] = 'Disease Prediction not available for this crop.'
        except ObjectDoesNotExist:
            # if schedule with given id doesn't exist then display error
            context['status'] = False
            context['message'] = 'Invalid Id parameter.'
        except Exception as e:
            logger.error(str(e))
            context['status'] = False
            context['message'] = "Some error occurred. Please contact administration."

    # if csv format is required, then modify the response and return csv file
    if request.GET.get("format") and request.GET.get("format")=="csv":
        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=prediction.csv'
        if not context['status']:
            # if error set error message in context
            context['message'] = [context['message']]
        else:
            # modifying the context for csv format
            for p in context['prediction']:
                context[p[0]] = p[1]
            del context['prediction']
            del context['map']
        del context['status']
        # get a dataframe from 'context' dictionary
        df = pandas.DataFrame(context)
        # write dataframe to response with no indexing
        df.to_csv(response, index = False)
        # return csv file
        return response
    else:
        # else return HtmlResponse with a new parameter
        dt = datetime.datetime.now()
        context['week'] = [ (dt + datetime.timedelta(days=i)).strftime("%d-%b") for i in range(7) ]
        return render(request, 'app/dp.html', context)

@login_required
def nutrition(request):
    """
    Returns NPK recommendation for the schedule whose id is given

    Args: HttpRequest

    Returns: HttpResponse(csv/json)
    """
    context = {}
    id = request.GET.get("id")
    if id:
        # this method returns the NPK recommendation for the schedule whose id is given as parameter
        result = nutrition_modified(id)
        if result['status']:
            # if success then set these data lists in context
            context['xp2o5c'], context['yp2o5c'] = zip(*result['p2o5c'])
            context['xk2oc'], context['yk2oc'] = zip(*result['k2oc'])
            context['xnc'], context['ync'] = zip(*result['nc'])
        else:
            # else set error message in context
            context['error'] = [result['message']]
    else:
        # if id is not specified set following error message in context
        context['error'] = ["No schedule id specified"]

    if request.GET.get("format") and request.GET.get("format")=="csv":
        # if csv format is required then modify the response and return csv
        response = HttpResponse(content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=nutrition.csv'
        if not context.get('error'):
            # if success then add these data arrays into a dict
            context = dict(xP2O5 = np.array(context['xp2o5c']).astype(int), yP2O5 = np.array(context['yp2o5c']), xK2O = np.array(context['xk2oc']).astype(int), yK2O = np.array(context['yk2oc']), xN = np.array(context['xnc']).astype(int), yN = np.array(context['ync']))
        # convert dictionary into a pandas dataframe
        df = pandas.DataFrame(dict([ (k,pandas.Series(v)) for k,v in context.items() ]))
        # write dataframe to response with no indexing
        df.to_csv(response, index = False)
        return response
    else:
        # else return json response
        return HttpResponse(json.dumps(context), content_type="application/json")
