# Run this cell to mount your Google Drive.
import os
import tensorflow as tf
from keras.models import load_model
from keras.optimizers import RMSprop
from .segment import *
from collections import OrderedDict
import numpy as np
global graph
graph = tf.compat.v1.get_default_graph()

labels = ['Apple___Apple_scab', 'Apple___Black_rot', 'Apple___Cedar_apple_rust', 'Apple___healthy', 'Blueberry___healthy', 'Cherry_(including_sour)___Powdery_mildew', 'Cherry_(including_sour)___healthy', 'Corn_(maize)___Cercospora_leaf_spot Gray_leaf_spot', 'Corn_(maize)___Common_rust_', 'Corn_(maize)___Northern_Leaf_Blight', 'Corn_(maize)___healthy', 'Grape___Black_rot', 'Grape___Esca_(Black_Measles)', 'Grape___Leaf_blight_(Isariopsis_Leaf_Spot)', 'Grape___healthy', 'Orange___Haunglongbing_(Citrus_greening)', 'Peach___Bacterial_spot', 'Peach___healthy', 'Pepper,_bell___Bacterial_spot', 'Pepper,_bell___healthy', 'Potato___Early_blight', 'Potato___Late_blight', 'Potato___healthy', 'Raspberry___healthy', 'Soybean___healthy', 'Squash___Powdery_mildew', 'Strawberry___Leaf_scorch', 'Strawberry___healthy', 'Tomato___Bacterial_spot', 'Tomato___Early_blight', 'Tomato___Late_blight', 'Tomato___Leaf_Mold', 'Tomato___Septoria_leaf_spot', 'Tomato___Spider_mites Two-spotted_spider_mite', 'Tomato___Target_Spot', 'Tomato___Tomato_Yellow_Leaf_Curl_Virus', 'Tomato___Tomato_mosaic_virus', 'Tomato___healthy']

model = load_model(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))),'plant_villlage_trained_model_inception.h5'))
model.compile(loss='categorical_crossentropy', optimizer=RMSprop(lr=0.0001,decay=1e-6),metrics=['accuracy'])

def predict(image_source):
    """
    predict the Probabilities after segmenting the image

    Args:
        image_source: path to image file

    Returns:
        Tuple of dictionary (disease: Probability) and max Probability
    """
    image = segmentImage(image_source)
    image = image.astype('float32')
    image /= 255.
    image = np.expand_dims(image, axis=0)
    # use the globally declared graph (as default)
    # because the model was loaded and compiled in that
    with graph.as_default():
        prediction = model.predict(image)
        prediction[np.abs(prediction)<1e-5] = 0
        probab = np.amax(prediction)
        prediction = prediction.tolist()[0]
        prediction = dict(zip(labels, prediction))
        prediction = [(k,v) for k, v in sorted(prediction.items(), key=lambda x: x[1], reverse = True)]
        prediction = OrderedDict(prediction)
        return prediction, probab
