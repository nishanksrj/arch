from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from ..forms import *
from ..models import *

import requests, logging, datetime

logger = logging.getLogger('django_log')


@login_required
def plots_table(request):
    """
    Display a table containing brief info of all plots
    sorted in order of date of creation

    Args: HttpRequest Object

    Returns: HttpResponse Object
    """
    context = {}
    plots = Plot.objects.all().order_by('-created')
    context['plots'] = plots
    return render(request, 'app/plots_table.html', context)


@login_required
def plot(request, id):
    """
    Display a plot's properties whose id is passed as parameter

    Args: HttpRequest Object and database id of a plot

    Returns: HttpResponse Object
    """
    context = {}
    # extract plot with given id
    context['plot'] = Plot.objects.get(id=id)

    # get geo_json from db and convert into a string with required format
    context['linestring'] = "["
    coord_json = json.loads(context['plot'].coord_json)
    for point in coord_json['coordinates'] + [coord_json['coordinates'][0]]:
        context['linestring'] = context['linestring'] + str(point['lat']) + ',' + str(point['lng']) + ', 100,'
    context['linestring'] = context['linestring'] + ']'
    context['lastLat'] = coord_json['coordinates'][0]['lat']
    context['lastLng'] = coord_json['coordinates'][0]['lng']
    try:
        ### get the ndvi data for the plot from agromonitoring api using geo_id
        ### which is a property of plot instance created at the time of plot creation
        url = 'https://api.agromonitoring.com/agro/1.0/image/search?polyid=' + context['plot'].geo_id + '&start=1556668800&end=1559347200&appid=f64d048d626bdee2e7b518e54f4f4a96'
        response = requests.get(url)
        json_data = response.json()
        context['ndvi_image_url'] = json_data[0]['image']['ndvi']
        ndvi_stats_url = json_data[0]['stats']['ndvi']
        response = requests.get(ndvi_stats_url)
        json_data = response.json()
        # set these parameters in context
        context['num'] = json_data['num']
        context['min'] = "%.2f"%json_data['min']
        context['max'] = "%.2f"%json_data['max']
        context['mean'] = "%.2f"%json_data['mean']
    except:
        messages.error(request, "Can not access NDVI data.")
    try:
        ### check if a schedule exists for this plot or not
        ### if exists display "Show Schedule" button
        ### else display "Add Schedule" button
        context['schedule'] = Schedule.objects.get(plot = context['plot'])
    except Exception as e:
        pass
    return render(request, 'app/plot_profile.html', context)


def weather(request):
    """
    Display current and forecast weather in a template

    Args: HttpRequest Object with 'lat' and 'lng' as GET Request parameters

    Returns: HttpResponse Object
    """
    context = {}
    try:
        # extract lat, lng from request query
        lat = request.GET.get('lat')
        lng = request.GET.get('lng')
        # using world weather online's api to get weather forecast
        url = 'https://api.worldweatheronline.com/premium/v1/weather.ashx?key=8dbe0cc0d3804ecaac073017192905&q='+str(lat) + ',' + str(lng) + '&num_of_days=7&cc=no&mca=no&tp=1&format=json'
        response = requests.get(url)
        js = response.json()
        js = js.get('data').get('weather')
        # initialize lists to store avg daily data for 7 days
        Tavg=[]
        # avg Wind Speed
        Uavg=[]
        # avg Relative Humidity
        RHavg=[]
        Dates = []
        for j in range(7):
            rh = 0
            u = 0
            # take avg of hourly data of that day
            for h in js[j]['hourly']:
                rh+= float(h['humidity'])
                u+= float(h['windspeedKmph'])
            # add averaged data to the list
            RHavg.append("%.2f"%(rh/24.0))
            Uavg.append("%.2f"%(u/24.0))
            # avg temp calculation not needed as api directly provides daily avg temp
            Tavg.append(js[j]['avgtempC'])
            # also create a list of dates for next 7 days in 'DD Mon' format (alternative possible using js)
            Dates.append((datetime.date.today()+datetime.timedelta(days=j)).strftime('%d %b'))

        # set these parameters in context
        context['Date'] = Dates
        context['today'] = (datetime.date.today().strftime('%d %b, %Y'), Tavg[0], RHavg[0], Uavg[0])
        context['zip'] = zip(Dates[1:], Tavg[1:], RHavg[1:], Uavg[1:])
    except Exception as e:
        # log the error
        logger.error(e)
        # set error message
        messages.error(request, "Some error occurred. Please try again later or contact administration.")
    return render(request, 'app/weather.html', context)


@login_required
def soil_report(request, id):
    """
    Handles excel file uploads
    and updates plot's soil nutrient data

    Args: HttpRequest Object and database id of the plot

    Returns: HttpRespnose Object
    """
    if request.method=="POST":
        form = DataFileForm(request.POST, request.FILES)
        # check form validity
        if form.is_valid():
            try:
                datafile = form.cleaned_data["datafile"].file
                # open excel file using openpyxl module
                wb = openpyxl.load_workbook(datafile)
                # get the first sheet
                sheet = wb[wb.sheetnames[0]]
                # get the plot with given id
                plot = Plot.objects.get(id = id)
                # update soil properties to new values
                plot.Organic_Carbon = sheet.cell(16,6).value
                plot.pH = sheet.cell(17,6).value
                plot.Electrical_Conductivity = sheet.cell(18,6).value
                plot.Sodium = sheet.cell(19,6).value
                plot.Nitrogen = sheet.cell(20,6).value
                plot.Phosphorus = sheet.cell(21,6).value
                plot.Potassium = sheet.cell(22,6).value
                plot.Calcium = sheet.cell(23,6).value
                plot.Magnesium = sheet.cell(24,6).value
                plot.Sulphur = sheet.cell(25,6).value
                plot.Copper = sheet.cell(26,6).value
                plot.Iron = sheet.cell(27,6).value
                plot.Zinc = sheet.cell(28,6).value
                plot.Manganese = sheet.cell(29,6).value
                plot.Boron = sheet.cell(30,6).value
                plot.Cation_Exchange_Capacity = sheet.cell(31,6).value
                plot.Water_Holding_Capacity = sheet.cell(35,6).value
                plot.Bulk_Density = sheet.cell(36,6).value
                plot.Particle_Density = sheet.cell(37,6).value
                plot.Sand = sheet.cell(41,6).value
                plot.Silt = sheet.cell(42,6).value
                plot.Clay = sheet.cell(43,6).value
                # save the changes
                plot.save()
                # set success message
                messages.success(request,'Data updated successfully.')
            except:
                # set error message
                messages.error(request,'Some error occurred. Please contact administration.')
    return redirect('/dashboard/plot/'+str(id)+ '/info')


# @login_required
# def make_polygon(request,id):
#     try:
#         plot = Plot.objects.get(id = id)
#         topLeftLat = request.GET.get("topLeftLat")
#         topLeftLng = request.GET.get("topLeftLng")
#         bottomRightLat = request.GET.get("bottomRightLat")
#         bottomRightLng = request.GET.get("bottomRightLng")
#         geo_json = {
#                     "name": plot.plot_name,
#                     "geo_json":{
#                         "type":"Feature",
#                         "properties":{},
#                         "geometry":{
#                             "type": "Polygon",
#                             "coordinates":[
#                                 [
#                                     [float(topLeftLng), float(topLeftLat)],
#                                     [float(bottomRightLng), float(topLeftLat)],
#                                     [float(bottomRightLng), float(bottomRightLat)],
#                                     [float(topLeftLng), float(bottomRightLat)],
#                                     [float(topLeftLng), float(topLeftLat)]
#                                 ]
#                             ]
#                         }
#                     }
#                 }
#         headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
#         r = requests.post('http://api.agromonitoring.com/agro/1.0/polygons?appid=f64d048d626bdee2e7b518e54f4f4a96', headers = headers, data=json.dumps(geo_json))
#         if r.status_code==201:
#             js = r.json()
#             plot.geo_id = js['id']
#             plot.save()
#         return HttpResponse("Success")
#     except Exception as e:
#         return HttpResponse("Error")
