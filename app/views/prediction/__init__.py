from .train_model import *
import numpy as np


def predict(Crop, Stage, l):
    temp = []
    temp1=[]
    i=0
    if(Crop=='Hot Pepper (Chilli)'):
        map = ['Damping Off', 'Fruit Rot and Die Back', 'Powdery Mildew', 'Bacterial Leaf Spot', 'Cercospora Leaf Spot', 'Fusarium Wilt']
        if(Stage=='Seedling'):
            for var in l:
                temp.append(chilli_seedling.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
              var=var[0].tolist()
              var=[var[0]]+[0]+var[1:]
              temp1.insert(i,var)
              i=i+1

        elif (Stage=='Flowering'):
            for var in l:
                temp.append(chilli_flowering.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
              var=var[0].tolist()
              var=[0]+var
              temp1.insert(i,var)
              i=i+1
        else:
            for var in l:
                temp.append(chilli_all_other.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
              var=var[0].tolist()
              var=[0,0]+var
              temp1.insert(i,var)
              i=i+1
    elif(Crop=='Tomato'):
        map = [ 'Damping Off', 'Septorial Leaf Spot', 'Bacterial Stem and Fruit Canker', 'Early Blight', 'Bacterial Leaf Spot']
        if(Stage=='Seedling'):
            for var in l:
                temp.append(tomato_seedling.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
              var=var[0].tolist()
              var=[var[0]]+[0,0]+[var[1]]+[0]
              temp1.insert(i,var)
              i=i+1
        else:
            for var in l:
                temp.append(tomato_all_other.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
              var=var[0].tolist()
              var=[0]+var
              temp1.insert(i,var)
              i=i+1
    elif(Crop=='Cotton'):
        map = ['Fusarium Wilt', 'Root Rot', 'Anthracnose', 'Alternia Leaf Spot', 'Bacterial Blight']
        if(Stage=='Seedling'):
            for var in l:
                temp.insert(i,cotton_seedling.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
                var=var[0].tolist()
                var=[var[0],var[1]]+[0]+var[2:]
                temp1.insert(i,var)
                i=i+1
        elif (Stage=='Flowering'):
            for var in l:
                temp.insert(i,cotton_flowering.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
                var=var[0].tolist()
                var=[var[0]]+[0]+var[1:]
                temp1.insert(i,var)
                i=i+1
        else:
            for var in l:
                temp.insert(i,cotton_all_other.predict_proba(np.asarray(var).reshape(1,-1)))
            for var in temp:
                var=var[0].tolist()
                var=[var[0]]+[0,0]+[var[1]]+[0]+[var[2]]
                temp1.insert(i,var)
                i=i+1
    return temp1, map

def get_prediction(crop, lat, lng, stage='other'):
    response = {}
    try:
        url = 'https://api.worldweatheronline.com/premium/v1/weather.ashx?key=8dbe0cc0d3804ecaac073017192905&q='+str(lat) + ',' + str(lng) + '&num_of_days=7&cc=no&mca=no&tp=1&format=json'
        data = requests.get(url)
        js = data.json()
        js = js.get('data').get('weather')
        Tavg=[]
        RHavg=[]
        Dates = []
        for j in range(7):
            rh = 0
            for h in js[j]['hourly']:
                rh+= float(h['humidity'])
            Tavg.append(js[j]['avgtempC'])
            RHavg.append("%.2f"%(rh/24.0))
        z = list(zip(RHavg, Tavg))
        response['prob'], response['map'] = predict(crop, stage, z)
        response['status'] = True
    except Exception as e:
        response['status'] = False
        response['error'] = str(e)
    return response
