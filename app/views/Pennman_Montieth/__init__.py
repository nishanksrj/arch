import pandas as pd
import numpy as np
import math

from scipy.signal import savgol_filter
from scipy.interpolate import interp1d, CubicSpline

import datetime
from dateutil.relativedelta import relativedelta
import requests
import io, os
Rh_max = 0.0
Rh_min = 0.0
T_mean = 0.0
slight_duration = 0.0
wind_vel = 0.0
ud_un_ratio = 0.0
latitude = 0.0
longitude = 0.0
altitude = 0.0
month = 0
day_number = 0
crop_id = 0
irrig_freq = 0
Rh_mean = 0.0
dcsv = []

base_directory = os.path.dirname(os.path.abspath(__file__))

def func_crop_coeff(irrig_freq_input,ref_evapo_transp,Rh_min,wind_vel,crop_id,day_number):
    irrig_freq = str(irrig_freq_input) + ' Days'
    table21 = pd.read_csv(os.path.join(base_directory,'Table_21.csv'))
    if(Rh_min > 70):
        #print('Relative Humidity is high.')
        rel_table21 = table21.loc[table21['Rh Min.'] == 'High']
    else:
        #print("Relative Humidity is moderate.")
        rel_table21 = table21.loc[table21['Rh Min.'] == 'Moderate']
    if(wind_vel>5):
        #print('Wind Velocity is high.')
        rel_table21 = rel_table21.loc[rel_table21['Wind Speed'] == 'High']
    else:
        #print('Wind Velocity is moderate.')
        rel_table21 = rel_table21.loc[rel_table21['Wind Speed'] == 'Moderate']

    #print(rel_table21.columns[crop_id - 1])
    crop_coeff_3 = rel_table21[rel_table21.columns[crop_id - 1]].values[0]
    crop_coeff_4 = rel_table21[rel_table21.columns[crop_id - 1]].values[1]

    fig6 = pd.read_csv(os.path.join(base_directory,'figure_6_table.csv'))
    #print(fig6[irrig_freq])
    fig6_x = fig6['ref_evapo_transp'].values.reshape(10)
    fig6_y = fig6[irrig_freq].values.reshape(10)
    fig6_i = interp1d(fig6_x,fig6_y,kind = 'linear')
    if(ref_evapo_transp > 10):
        print("EEEEEEEEEEEEEEEEEEEEEEEE_Et0_Warning")
        print(ref_evapo_transp)
        ref_evapo_transp = 10
    crop_coeff_1 = round(float(fig6_i(ref_evapo_transp)),2)
    crop_coeff_graph_data_x = [10,20,55,75,95,110]
    crop_coeff_graph_data_y = [crop_coeff_1,crop_coeff_1,crop_coeff_3,crop_coeff_3,crop_coeff_3,crop_coeff_4]
    crop_coeff_graph_data_y_poly_fit_obj = np.polyfit(crop_coeff_graph_data_x,crop_coeff_graph_data_y,4)
    crop_coeff_graph_data_y_poly_fit = np.poly1d(crop_coeff_graph_data_y_poly_fit_obj)
    #print(crop_coeff_graph_data_y_poly_fit(day_number))
    return crop_coeff_graph_data_y_poly_fit(day_number)



def get_vap_press(T):
    T_deno = float(T) + 237.3
    T_ratio = 17.27*float(T)/T_deno
    val = 0.6108*math.exp(T_ratio)
    return val

def get_psycho_const(altitude):
    at_press = 101.3*(((293.0 - 0.0065*altitude)/293.0)**5.26)
    sp_heat = 1.013
    mol_wt = 0.622
    lat_heat_vap = 2.45
    psy_const = (sp_heat*at_press)/(mol_wt*lat_heat_vap*1000)
    return psy_const

def get_sigma_T4(T):
    T = T + 273.16
    sigma = 4.903*(10**(-9))
    val = sigma*(T**4)
    return val

def get_Ra(month,latitude,alph_month):
    table10 = pd.read_csv(os.path.join(base_directory,'Table_10.csv'))
    table10_i = interp1d(table10['Latitude'].values.reshape(26),table10[alph_month[month - 1]].values.reshape(26),kind = 'linear')
    if(latitude <=50):
        #print("Taking Ra from Table 10")
        Ra = table10_i(latitude)
    else:
        print("ERROR : Ra can't be taken from Table 10")
    return Ra*2.45

def get_N(month,latitude,alph_month):
    table11 = pd.read_csv(os.path.join(base_directory,'Table_11.csv'))
    table11_i = interp1d(table11['Latitude'].values.reshape(14),table11[alph_month[month - 1]].values.reshape(14),kind = 'linear')
    if(latitude <= 50):
        #print("Taking N from Table 11")
        N = table11_i(latitude)
    return N

def func_ref_evapo_transp(Rh_data,T_data,slight_dur,wind_vel_mps,latitude,altitude,month):
    #print(Rh_data,T_data,slight_dur,wind_vel_mps,latitude,altitude,month)
    Rh_max,Rh_mean,Rh_min = Rh_data[0] , Rh_data[1] , Rh_data[2]
    T_max,T_min,T_mean,T_month,T_prev_month = T_data[0] , T_data[1] , T_data[2] , T_data[3] , T_data[4]
    alph_month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    T_deno = float(T_mean) + 237.3

    #Algorithm
    delta = 4098*get_vap_press(T_mean)/(T_deno**2)
    psy_const = get_psycho_const(altitude)
    es = (get_vap_press(T_max) + get_vap_press(T_min))/2.0
    ea = ((get_vap_press(T_max)*Rh_min) + (get_vap_press(T_min)*Rh_max))/200.0
    #print(get_vap_press(T_max))
    #print(get_vap_press(T_min))
    N = get_N(month,latitude,alph_month)
    Ra = get_Ra(month,latitude,alph_month)
    Rs = (0.25 + 0.5*slight_dur/N)*Ra
    Rns = 0.77*Rs
    Rso = (0.75 + 2*(altitude)/100000)*Ra
    Rnl_fac1 = (get_sigma_T4(T_max) + get_sigma_T4(T_min))/2.0
    Rnl_fac2 = 0.34 - 0.14*math.sqrt(ea)
    Rnl_fac3 = (1.35*Rs/Rso) - 0.35
    Rnl = Rnl_fac1*Rnl_fac2*Rnl_fac3
    Rn = Rns - Rnl
    G = 0.14*(T_month - T_prev_month)
    ref_evapo_transp_fac1 = 0.408*delta*(Rn-G)/(delta + psy_const*(1 + 0.34*wind_vel_mps))
    ref_evapo_transp_fac2 = 900*psy_const*wind_vel_mps*(es - ea)/((delta + psy_const*(1 + 0.34*wind_vel_mps))*T_deno)
    ref_evapo_transp = ref_evapo_transp_fac1 + ref_evapo_transp_fac2
    #print(delta,psy_const,es,ea,Ra,N,Rs,Rns,Rso,Rnl_fac1,Rnl_fac2,Rnl_fac3,Rnl,Rn,G)
    #print(ref_evapo_transp)
    return ref_evapo_transp





def pastWeatherExtraction(lat, lng):
    cdate = datetime.datetime(2018, 4, 1)
    ndate = cdate + relativedelta(months=+1)
    for i in range(6):
        url = 'http://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=8dbe0cc0d3804ecaac073017192905&q='+ str(lat) +',' + str(lng) + '&date='+ cdate.strftime('%d-%m-%Y') + '&enddate='+ (ndate-datetime.timedelta(days=1)).strftime('%d-%m-%Y') + '&format=csv'
        response = requests.get(url)
        if response.status_code==200:
            data = response.content.decode('utf-8').split('\n')
            del data[0:5]
            del data[1:3]
            del data[-1]
            data = "\n".join(data)
            dcsv.append(pd.read_csv(io.StringIO(data)))
            cdate = ndate
            ndate = ndate + relativedelta(months=+1)
        else:
            return False
    return True


def get_live_weather(day_number):
    if(day_number <= 14):
        i = 1
    elif(day_number <= 44):
        i = 2
        day_number -= 14
    elif(day_number <= 75):
        i = 3
        day_number -= 44
    elif(day_number <= 106):
        i = 4
        day_number -= 75
    elif(day_number <= 123):
        i = 5
        day_number -= 106
    else:
        print("Day Number out-of-bounds")
    data_prev = dcsv[i-1]
    data = dcsv[i]
    data_days_prev = data_prev.loc[data_prev.isnull().any(axis=1)]
    temp_max_month_prev = np.average(data_days_prev.iloc[:,1:2].values)
    temp_min_month_prev = np.average(data_days_prev.iloc[:,3:4].values)
    temp_month_prev = (temp_max_month_prev + temp_min_month_prev)/2.0
    data_days = data[pd.isnull(data).any(axis=1)]
    data_days = data.loc[data['FeelsLikeC'] != data['FeelsLikeC']]
    temp_max_month = np.average(data_days.iloc[:,1:2].values)
    temp_min_month = np.average(data_days.iloc[:,3:4].values)
    temp_month = (temp_max_month + temp_min_month)/2.0

    data_hours = data.loc[data['FeelsLikeC'] == data['FeelsLikeC']]
    data_thatday = data_hours.iloc[8*(day_number-1):8*(day_number),:]
    temp_max = np.max(data_thatday.iloc[:,2:3].values)
    temp_min = np.min(data_thatday.iloc[:,2:3].values)
    temp_mean = np.average(data_thatday.iloc[:,2:3].values)
    u_night_mean = (np.sum(data_thatday.iloc[0:2,5:6].values.astype(float)) + np.sum(data.iloc[6:8,5:6].values.astype(float)))*12.0
    u_day_mean = np.average(data_thatday.iloc[2:6,5:6].values.astype(float))*24.0
    #print(u_day_mean,u_night_mean)
    wind_mean = (u_day_mean + u_night_mean)/2.0
    rel_humid_mean = np.average(data_thatday.iloc[:,13:14].values)
    rel_humid_max = np.max(data_thatday.iloc[:,13:14].values)
    rel_humid_min = np.min(data_thatday.iloc[:,13:14].values)
    timestr_rise = str(data_days.iloc[day_number-1:day_number,5:6].values[0][0])
    hs = float(timestr_rise[0:2])
    ms = float(timestr_rise[3:5])
    tps = 0
    if(timestr_rise[6] == 'P'):
        tps = 12
    time_frac_rise = hs + ms/60.0 + tps
    timestr_fall = str(data_days.iloc[day_number-1:day_number,6:7].values[0][0])
    hf = float(timestr_fall[0:2])
    mf = float(timestr_fall[3:5])
    tpf = 0
    if(timestr_fall[6] == 'P'):
        tpf = 12
    time_frac_fall = hf + mf/60.0 + tpf
    slight = time_frac_fall - time_frac_rise
    return [temp_max,temp_min,temp_mean,temp_month,temp_month_prev],wind_mean,[rel_humid_max,rel_humid_mean,rel_humid_min],slight


def get_crop_id(name):
    data = pd.read_csv(os.path.join(base_directory,'crop_id.csv'))
    name_d = data.loc[data['Crop Name'] == name]
    id = name_d.values[0][1]
    return int(id)

def run_for_cycle(lat, lng, name):
    lat = float(lat)
    lng = float(lng)
    pastWeatherExtraction(lat, lng)
    irrig = []
    ref_irrig = []
    coeff = []
    days = []
    latitude,altitude,crop_id,irrig_freq = (lat, 100, get_crop_id(name), 7)
    wind_vel = 250*0.01157407407
    i = 1
    month = 5
    day_max_arr = [31,28,31,30,13,30,31,31,20,31,30,31]
    flag = 1
    while(i<=123):
        T_data,wind_vel,Rh_data,slight_duration = get_live_weather(i)
        wind_vel = wind_vel**0.01157407407
        ref_evapo_transp = func_ref_evapo_transp(Rh_data,T_data,slight_duration,wind_vel,latitude,altitude,month)
        crop_coeff = func_crop_coeff(irrig_freq,ref_evapo_transp,Rh_data[2],wind_vel,crop_id,i)
        irrigation_required = crop_coeff*ref_evapo_transp
        days.append(i)
        ref_irrig.append(ref_evapo_transp)
        coeff.append(crop_coeff)
        irrig.append(irrigation_required)
        if(flag > day_max_arr[month - 1]):
            flag = 1
            month +=1
        i+=1
        flag+=1

    irrig_hat = savgol_filter(irrig,27, 4)
    irrig_hat = irrig_hat.tolist()
    return (irrig, irrig_hat, days)
