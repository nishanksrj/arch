from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from ..forms import *
from ..models import *

import requests, logging

logger = logging.getLogger('django_log')


@login_required
def farmers_table(request):
    """
    Display a table containing info of all farmer,
    farmers are sorted in alphabetical order of their names

    Args: HttpRequest Object

    Returns: HttpResponse Object
    """
    context = {}
    context['farmers'] = Farmer.objects.all().order_by('-firstName')
    return render(request, 'app/farmers_table.html', context)


@login_required
def farmer(request, cid):
    """
    Display Farmer's Profile along with her/his plots and active schedules

    Args: HttpRequest Object

    Returns: HttpResponse Object
    """
    context = {}
    context['farmer'] = Farmer.objects.get(cid = cid)
    context['plots'] = Plot.objects.filter(farmer = context['farmer'])
    context['schedules'] = Schedule.objects.filter(plot__farmer = context['farmer'])
    return render(request, 'app/farmer_profile.html', context)
