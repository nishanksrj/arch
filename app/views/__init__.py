from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.db.models import Count

import io, os, json, logging
from ..forms import *
from ..models import *

from .plots import *
from .farmers import *
from .schedules import *
from .vision import *

logger = logging.getLogger('django_log')


def handler403(request, exception):
    """ 403 Error Handler """
    return render(request, 'app/page_403.html')

def handler404(request, exception):
    """ 404 Error Handler """
    return render(request, 'app/page_404.html')

def handler500(request):
    """ 500 Error Handler """
    return render(request, 'app/page_500.html')


def login_success(request):
    """
    Redirects users based on whether they are in admins group
    """
    if request.user.groups.filter(name="agent").exists():
        return redirect("/tag")
    else:
        return redirect('/')

@login_required
def dashboard(request):
    """
    default view (renders Dashboard)
    """
    context = {}
    context['farmers_count'] = Farmer.objects.all().count()
    context['plots_count'] = Plot.objects.all().count()
    context['schedule_count'] = Schedule.objects.all().count()
    context['crop_count'] = Crop.objects.all().count()
    context['state_farmers_count'] = Farmer.objects.all().values('state').annotate(total=Count('state')).order_by('state')
    context['state_plots_count'] = Plot.objects.all().values('farmer__state').annotate(total=Count('farmer__state')).order_by('farmer__state')
    context['crop_schedule_count'] = Schedule.objects.all().values('crop__name').annotate(total=Count('crop__name')).order_by('crop__name')
    return render(request, 'app/dashboard.html', context)



@login_required
def profile(request):
    """
    Display user profile

    Args: HttpRequest Object

    Returns: HttpResponse Object
    """
    context = {}
    context['user'] = request.user
    return render(request, 'app/profile.html', context)



@login_required
def check_disease(request):
    """
    Render html page to upload image

    Args: HttpRequest Object

    Returns: HttpResponse Object
    """
    return render(request, 'app/submit_disease.html')



@login_required
def disease_classification(request):
    """
    Show the predicted result

    Args: HttpRequest Object

    Returns: Json Response
    """
    context = {}
    if request.method=="POST":
        form = ImageFileForm(request.POST, request.FILES)
        if form.is_valid():
            # read image as a string
            image = form.cleaned_data["datafile"].read()
            # convert string image into np array
            image = np.fromstring(image, np.uint8)
            # pass the array into predict function which returns Probabilities of all diseases and max Probability among all diseases
            disease, prob = predict(image)
            # set these parameters in context
            context['disease'] = disease
            context['Probability'] = str(prob)
            context['status'] = True
        else:
            context['status'] = False
            # set error message
            context['message'] = 'Invalid entry'
    return HttpResponse(json.dumps(context), content_type="application/json")

def updateCropData(request):
    """
    Collect all active crops from genie api and
    write them in a 'choices.py' file so that the crops can be used in a select field

    Args: HttpRequest Object

    Returns: HttpResponse Object
    """
    try:
        url = 'https://popdev.agrostar.in/popservice/crop/?isActive=true'
        response = requests.get(url)
        crop_data = response.json()
        crops = crop_data['responseData']['cropList']
        f = open( os.path.join(os.path.dirname(os.path.abspath(__file__)),'choices.py'),'w')
        f.write('CROPS = (')
        for crop in crops:
            f.write("('" + crop['uuid'] + "', '" + crop['cropName'] + "'),\n")
            try:
                crop = Crop.objects.get(uuid = crop['uuid'])
            except ObjectDoesNotExist:
                crop = Crop(uuid = crop['uuid'], name = crop['cropName'], imgUrl = crop['imageUrl'])
                crop.save()
        f.write(')')
        f.close()
        return HttpResponse("Task successfully completed.")
    except Exception as e:
        return HttpResponse(str(e))
