import pandas as pd
import math
from ...models import Schedule

factorplnt = 1
fac = 1

factorpl = [0]*5
facteur = [0]*5


NEXPa = [0]*4
kexpa = [0]*4
pexpa = [0]*4
caexpa = [0]*4
mgexpa = [0]*4
max = [0]*4
min = [0]*4
NPLNTA = [0]*4
pplnta = [0]*4
kplnta = [0]*4
caplnta = [0]*4
mgplnta = [0]*4
pfactora = [0]*4
cropa = [0]*4

NFA = [0, 1.2, 1.2, 1.2, 1.2, 1.2]
PFA = [0, 1.8, 1.5, 1.2, 0.5, 0]
KFA = [0, 2, 1.6, 1.3, 1, 0.5]
CAFA = [0, 2, 1.5, 0.5, 0, 0]
MGFA = [0, 2, 1.5, 1, 0.5, 0]
NITRATE = [0, 5, 8, 10, 15, 20]


NFACT=NFA[3]
PFACT=PFA[3]
KFACT=KFA[3]
CAFACT=CAFA[3]
MGFACT=MGFA[3]


NEXPa[1] = 14
pexpa[1] = 5
kexpa[1] = 7
caexpa[1] = 2
mgexpa[1] = 1.6

NPLNTA[1]=14
max[1]=8
min[1]=2
pplnta[1]=5
kplnta[1]=15
caplnta[1]=10
mgplnta[1]=3
pfactora[1]=1.4
cropa[1]='cotton'


NEXPa[2] = 2
pexpa[2] = .6
kexpa[2] = 3.5
caexpa[2] = 0.5
mgexpa[2] = 0.3

NPLNTA[2]=1.8
max[2]=200
min[2]=15
pplnta[2]=.4
kplnta[2]=2.3
caplnta[2]=1.9
mgplnta[2]=.5
pfactora[2]=2
cropa[2]='pepper'


NEXPa[3] = 1.5
pexpa[3] = .4
kexpa[3] = 2.8
caexpa[3] = 0.15
mgexpa[3] = 0.2

NPLNTA[3]=1.2
max[3]=240
min[3]=20
pplnta[3]=.3
kplnta[3]=1.9
caplnta[3]=2.2
mgplnta[3]=.5
pfactora[3]=2
cropa[3]='tomatoes'


Nrecy = 0
Precy = 0
Krecy = 0
Carecy = 0
Mgrecy = 0


factorpl[1]=1
facteur[1]=1
factorpl[2]=1.1
facteur[2]=1
factorpl[3]=1.2
facteur[3]=.7
factorpl[4]=1.4
facteur[4]=.5

def set_nutrition(crop_id, cyield):
    global fac
    global factorplnt
    if crop_id!=1:
        factorplnt = 1.2
        fac = 0.7
    vmin = min[crop_id]
    vmax = max[crop_id]
    if fac!=0:
        vmin = vmin* fac
        vmax = vmax* fac
    if cyield<vmin or cyield>vmax:
        return {"result": "Yield is not in the range: " + str(vmin) + "-" + str(vmax)}
    else:
        NEXP = NEXPa[crop_id]*cyield-0.1
        P2O5EXP = pexpa[crop_id]*cyield-0.1
        K20EXP = kexpa[crop_id]*cyield-0.1
        CAOEXP = caexpa[crop_id]*cyield-0.1
        MGOEXP = mgexpa[crop_id]*cyield-0.1
        if fac!=0:
            tikun = (cyield - ((vmax + (vmin/fac)) / 2)) * 0.3
        else:
            tikun = 0
        NPLNT = NPLNTA[crop_id] * (cyield - tikun) * factorplnt
        P2O5PLNT = pplnta[crop_id] * (cyield - tikun) * factorplnt
        K2OPLNT = kplnta[crop_id] * (cyield - tikun) * factorplnt
        CAOPLNT = caplnta[crop_id] * (cyield - tikun) * factorplnt
        MGOPLNT = mgplnta[crop_id] * (cyield - tikun) * factorplnt

        NUPTK =  NEXP + NPLNT
        P2O5UPTK = P2O5EXP + P2O5PLNT
        K2OUPTK = K20EXP + K2OPLNT
        CAOUPTK = CAOEXP + CAOPLNT
        MGOUPTK = MGOEXP + MGOPLNT

        NREC = NUPTK * NFACT
        if NREC < 0:
            NREC = 0
        PREC = P2O5UPTK * PFACT + ((pfactora[crop_id] - 1) * 106)
        if PREC < 0:
            PREC = 0

        KREC = K2OUPTK * KFACT
        if KREC < 0:
            KREC = 0

        CAREC = CAOUPTK * CAFACT
        if CAREC < 0:
            CAREC = 0

        MGREC = (MGOUPTK * MGFACT)
        if MGREC < 0:
            MGREC = 0
    return {'N_Rec': NREC, 'P_Rec': PREC, 'K_Rec': KREC, 'Ca_Rec': CAREC, 'Mg_Rec': MGREC}


def nutrition_modified(schedule_id):
    schedule = Schedule.objects.get(id=schedule_id)
    if schedule.crop.name=="Hot Pepper (Chilli)":
        crop_id = 2
    elif schedule.crop.name=="Tomato":
        crop_id = 3
    else:
        return {"message": "NPK recommendation not available for this crop.", "status": False}
    PH = schedule.plot.pH
    OrgC = schedule.plot.Organic_Carbon
    TotalP = schedule.plot.Phosphorus
    ExchK = schedule.plot.Potassium
    T = 30
    clay_percentage = schedule.plot.Clay
    density = schedule.plot.Bulk_Density


    ######### matching the units
    OrgC = OrgC * 10
    OrgN = OrgC / 10
    TotalP = TotalP * 0.656 / density

    SN = (45 * OrgN * pow(2, (T/9 - 1)))/math.log(15*clay_percentage,10)
    if SN<0:
        SN = 0

    SP = (0.0375 * TotalP + 0.45 * OrgC) * (1 - 0.25 * (pow(PH - 6.7, 2)))
    if SP<0:
        SP = 0

    SK = 0.35 * (2 + ExchK) * (55 - OrgC)
    if SK<0:
        SK = 0


    S = [SN, SP, SK]
    U = [0, 0, 0]
    c1 = [-0.05, -1.15, -0.35]
    c2 = [-0.35, -0.40, -0.07]
    for i in range(3):
        if ((S[(i+1) % 3] == 0) or (S[(i+2) % 3] == 0)):
            U[i%3] = 0
        elif ((S[(i+1) % 3] > 0) and (S[(i+2) % 3] > 0) and (S[i % 3] > 1/(-0.5 * ((c1[i%3]/S[(i+1)%3]) + (c2[i%3]/S[(i+2)%3]))))):
            U[i%3] = -0.368 / (0.5 * ((-0.5 * ((c1[i%3]/S[(i+1)%3]) + (c2[i%3]/S[(i+2)%3])))))
        elif ((S[(i+1) % 3] > 0) and (S[(i+2) % 3] > 0) and (S[i % 3] < 1/(-0.5 * ((c1[i%3]/S[(i+1)%3]) + (c2[i%3]/S[(i+2)%3]))))):
            U[i%3] = S[i] * pow(2.71,(0.5 * (c1[i%3] * S[i%3] / S[(i+1)%3]) + (c2[i%3] * S[i%3] / S[(i+2)%3])))
        else:
            continue

    ####### new inputs (for 'x' yeild)
    data = set_nutrition(crop_id, 18)

    RecN = data['N_Rec'] - U[0]
    RecP2O5 = data['P_Rec'] - U[1] * 7.0 / 3.0
    RecK2O = data['K_Rec'] - U[2]* 92.0 / 76.0

    plot_area = float(schedule.plot.areaHa)

    RecN*= plot_area
    RecP2O5*= plot_area
    RecK2O*= plot_area
    
    if schedule.crop.name=="Hot Pepper (Chilli)":
        df=pd.read_csv('https://raw.githubusercontent.com/nishanksrj/BitBat/master/chilli_nutrition_curve.csv', sep=',')
        P2O5 = zip(df['xP2O5'].values.tolist()[:56],df['yP2O5'].multiply(RecP2O5).values.tolist()[:56])
        K2O = zip(df['xK2O'].values.tolist()[:323],df['yK2O'].multiply(RecK2O).values.tolist()[:323])
        N = zip(df['xN'].values.tolist()[:104],df['yN'].multiply(RecN).values.tolist()[:104])
    elif schedule.crop.name=="Tomato":
        df=pd.read_csv('https://raw.githubusercontent.com/nishanksrj/BitBat/master/tomato_nutrition_curve.csv', sep=',')
        P2O5 = zip(df['xP2O5'].values.tolist()[:81],df['yP2O5'].multiply(RecP2O5).values.tolist()[:81])
        K2O = zip(df['xK2O'].values.tolist()[:84],df['yK2O'].multiply(RecK2O).values.tolist()[:84])
        N = zip(df['xN'].values.tolist()[:70],df['yN'].multiply(RecN).values.tolist()[:70])
    return {'p2o5c': P2O5, 'k2oc': K2O, 'nc': N, 'status': True}
    ## converting factor for chilli is (recomended value of respective nutrient / area under the curve for respective nutrient from the daataset we have provided)
