from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator
from django.utils.safestring import mark_safe
from django.core.exceptions import ValidationError

import math, json

class Farmer(models.Model):
    cid = models.BigIntegerField(primary_key = True)
    mobile1 = models.CharField(max_length = 10)
    mobile2 = models.CharField(max_length = 10, blank = True, null = True)
    mobile3 = models.CharField(max_length = 10, blank = True, null = True)
    firstName = models.CharField(max_length = 20)
    middleName = models.CharField(max_length = 20, blank = True, null = True)
    lastName = models.CharField(max_length = 20, blank = True, null = True)
    village = models.CharField(max_length = 50)
    taluka = models.CharField(max_length = 50)
    district = models.CharField(max_length = 50)
    state = models.CharField(max_length = 20)
    pincode_regex = RegexValidator(regex=r'^\d{6}$', message="Pincode must be of 6 digits.")
    pincode = models.CharField(validators = [pincode_regex], max_length = 6)
    date_joined = models.DateTimeField(default = timezone.now)

    def __str__(self):
        name = self.firstName
        if self.middleName:
            name = name + ' ' + self.middleName
        if self.lastName:
            name = name + ' ' + self.lastName
        return name

class Plot(models.Model):
    farmer = models.ForeignKey(Farmer,to_field = 'cid', on_delete = models.CASCADE, related_name='plots')
    plot_name = models.CharField(max_length=20)
    coord_json = models.CharField(max_length=1024)
    geo_id = models.CharField(max_length=50)
    created = models.DateTimeField(default = timezone.now)
    pH = models.FloatField(blank=True,null=True, verbose_name = 'pH')
    Electrical_Conductivity  = models.FloatField(blank=True,null=True, verbose_name = 'Electrical Conductivity (mS/cm)')
    Organic_Carbon = models.FloatField(blank=True,null=True,verbose_name ='Organice Carbon (%)')
    Nitrogen = models.FloatField(blank=True,null=True, verbose_name = 'Nitrogen (kg/ha)')
    Phosphorus = models.FloatField(blank=True,null=True, verbose_name = 'Phosphorus (kg/ha)')
    Potassium = models.FloatField(blank=True,null=True, verbose_name = 'Potassium (kg/ha)')
    Sodium = models.FloatField(blank=True,null=True, verbose_name = 'Sodium (mg/Kg)')
    Calcium = models.FloatField(blank=True,null=True, verbose_name = 'Calcium (mg/Kg)')
    Magnesium = models.FloatField(blank=True,null=True, verbose_name = 'Magnesium (mg/Kg)')
    Sulphur = models.FloatField(blank=True,null=True, verbose_name = 'Sulphur (mg/Kg)')
    Iron = models.FloatField(blank=True,null=True, verbose_name = 'Iron (mg/Kg)')
    Copper = models.FloatField(blank=True,null=True, verbose_name = 'Copper (mg/Kg)')
    Zinc = models.FloatField(blank=True,null=True, verbose_name = 'Zinc (mg/Kg)')
    Manganese = models.FloatField(blank=True,null=True, verbose_name = 'Manganese (mg/Kg)')
    Boron = models.FloatField(blank=True,null=True, verbose_name = 'Boron (mg/Kg)')
    Cation_Exchange_Capacity = models.FloatField(blank=True,null=True, verbose_name = 'Cation Exchange Capacity (meq/100g)')
    Silt = models.FloatField(blank=True,null=True, verbose_name = 'Silt (%)')
    Sand = models.FloatField(blank=True,null=True, verbose_name = 'Sand (%)')
    Clay = models.FloatField(blank=True,null=True, verbose_name = 'Clay (%)')
    Water_Holding_Capacity = models.FloatField(blank=True,null=True, verbose_name = 'Water Holding Capacity (%)')
    Bulk_Density = models.FloatField(blank=True,null=True, verbose_name = 'Bulk Density (gm/cm3)')
    Particle_Density =models.FloatField(blank=True,null=True, verbose_name = 'Particle Density (gm/cm3)')

    def __areaAc(self):
        """
        calculate the area of polygon
        """
        area = 0.0
        d2r = math.pi/180.0
        radius = 6378137.0
        l = []
        coord = json.loads(self.coord_json)
        for point in coord['coordinates']:
            l.append([float(point['lat']), float(point['lng'])])
        if len(l)<=2:
            return 0
        else:
            for i in range(len(l)):
                p1 = l[i]
                p2 = l[(i+1)%len(l)]
                area= area+((p2[1]-p1[1])*d2r)*(2+math.sin(p1[0]*d2r)+math.sin(p2[0]*d2r))
            area = abs(area*radius*radius/2.0)
            return "%.3f"%(area*0.000247105)

    def __areaHa(self):
        """
        calculate the area of polygon
        """
        area = 0.0
        d2r = math.pi/180.0
        radius = 6378137.0
        l = []
        coord = json.loads(self.coord_json)
        for point in coord['coordinates']:
            l.append([float(point['lat']), float(point['lng'])])
        if len(l)<=2:
            return 0
        else:
            for i in range(len(l)):
                p1 = l[i]
                p2 = l[(i+1)%len(l)]
                area= area+((p2[1]-p1[1])*d2r)*(2+math.sin(p1[0]*d2r)+math.sin(p2[0]*d2r))
            area = abs(area*radius*radius/2.0)
            return "%.3f"%(area*0.0001)

    areaAc = property(__areaAc)
    areaHa = property(__areaHa)

    def __str__(self):
        return self.plot_name

class Crop(models.Model):
    uuid = models.CharField(max_length=32, primary_key = True)
    name = models.CharField(max_length=32)
    imgUrl = models.TextField()


class Schedule(models.Model):
    plot = models.OneToOneField(Plot, on_delete = models.CASCADE)
    crop = models.ForeignKey(Crop, on_delete = models.CASCADE)
    irrigation = models.CharField(max_length = 20)
    e_yield = models.FloatField()
    sdate = models.DateTimeField()
    no_plants = models.IntegerField(blank = True, null = True)
    r_spacing = models.FloatField(blank = True, null = True)
    p_spacing = models.FloatField(blank = True, null = True)
    created = models.DateTimeField(default = timezone.now)
