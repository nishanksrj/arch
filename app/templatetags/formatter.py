from django.template import Library
from django.utils.numberformat import format

register = Library()

@register.filter
def latLngFormatter(value, decimal=4):
    # format lat lng values to four decimal places
    return format(value, ".", decimal)

latLngFormatter.is_safe = True
