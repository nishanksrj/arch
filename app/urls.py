import django
from django.urls import include, path
from .views import *


urlpatterns = [
    path('', dashboard, name="dashboard"),
    path('profile/', profile, name="profile"),

    path('farmers/', farmers_table, name="farmers_table"),
    path('farmer/<cid>/', farmer, name="farmer"),

    path('plots/', plots_table, name="plots_table"),
    path('plot/<id>/info', plot, name="plot"),
    path('plot/<id>/schedule', schedule, name="schedule"),
    path('schedules/', schedules_table, name="schedules_table"),
    path('irrigation_schedule/', irrigation_schedule, name="irrigation_schedule"),
    path('nutrition/', nutrition, name="nutrition"),
    path('dp/', dp, name="dp"),

    path('soil_report/<id>', soil_report, name="soil_report"),
    path('check_disease', check_disease, name="check_disease"),
    path('disease_classification', disease_classification, name="disease_classification"),
    path('login_success', login_success, name="login_success"),
    path('weather', weather, name="weather"),
    path('updateCropData/', updateCropData, name="updateCropData"),
    # path('plot/<id>/updatePolygon', make_polygon, name="make_polygon"),
]
