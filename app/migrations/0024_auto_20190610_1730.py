# Generated by Django 2.2.1 on 2019-06-10 12:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0023_auto_20190610_1651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='farmer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='plots', to='app.Farmer'),
        ),
    ]
