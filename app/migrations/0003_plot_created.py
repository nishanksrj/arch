# Generated by Django 2.2.1 on 2019-05-24 13:01

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_farmer_plot'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2019, 5, 24, 13, 1, 49, 870647, tzinfo=utc)),
        ),
    ]
