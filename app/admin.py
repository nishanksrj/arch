from django import forms
from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.views.generic.base import TemplateView
from django.template.loader import render_to_string
from django.shortcuts import redirect

from .models import *
from .forms import *
import urllib, json
from urllib.parse import urlencode

import plotly.offline as opy
import plotly.graph_objs as go

import math

html_template_for_plot_map = """
    <div class="container" id="mapContainer" style="height:500px;"></div>
    <script>
        // Initialize the platform object:
        var platform = new H.service.Platform({
          'app_id': 'myawyUf5nPfe914BSGfp',
          'app_code': 'F2A2ZPi1ETd6lGpeBLmJqA',
          useHTTPS: true
        });

        var pixelRatio = window.devicePixelRatio || 1;

        // Obtain the default map types from the platform object
        var defaultLayers = platform.createDefaultLayers({
          tilesize: pixelRatio === 1?  256:512,
          ppi: pixelRatio === 1? undefined: 320,
        });

        // Instantiate (and display) a map object:
        var map = new H.Map(
          document.getElementById('mapContainer'),
          defaultLayers.normal.map,
          {
            center: { lng: 13.4, lat: 13.4 },
            pixelRatio: pixelRatio
          }
        );

        map.setBaseLayer(defaultLayers.satellite.xbase);

        var mapEvents = new H.mapevents.MapEvents(map);
        var behavior = new H.mapevents.Behavior(mapEvents);
        var ui = H.ui.UI.createDefault(map, defaultLayers);
        ui.removeControl('mapsettings');
        behavior.disable(H.mapevents.Behavior.WHEELZOOM);

        function drawPolygon(){
          window.linestring = new H.geo.LineString(%s);
          var newMapPolygon = new H.map.Polyline(window.linestring,{
            style: { 'strokeColor': 'red','lineWidth':4, 'lineCap': 'butt'}
          });
          map.addObject(newMapPolygon);
          map.setViewBounds(newMapPolygon.getBounds());
          window.linestring.eachLatLngAlt(function(lat, lng, alt){
            console.log(lat + " " + lng);
            map.addObject(new H.map.Circle(
                { lat: lat, lng: lng},
                3,
                {
                  style:{
                    strokeColor: 'white',
                    lineWidth: 2,
                    fillColor: 'black'
                  }
                }));
          });
        }
        drawPolygon();
    </script>"""

class WeatherGraph(TemplateView):

    def get_context_data(self, lat, lng,**kwargs):
        context = super(WeatherGraph, self).get_context_data(**kwargs)

        url ='https://weather.cit.api.here.com/weather/1.0/report.json?product=forecast_7days_simple&latitude=' + str(lat) + '&longitude=' + str(lng) + '&oneobservation=true&app_id=myawyUf5nPfe914BSGfp&app_code=F2A2ZPi1ETd6lGpeBLmJqA'
        response = urllib.request.urlopen(url)
        json_data = json.loads(response.read().decode('utf-8'))
        w_arr = json_data['dailyForecasts']['forecastLocation']['forecast']
        ht = []
        lt = []
        x = []
        for w in w_arr:
            ht.append(w['highTemperature'])
            lt.append(w['lowTemperature'])
            x.append(w['utcTime'])
        trace1 = go.Scatter(x=x, y=ht, marker={'color': 'orange', 'size': 10},  name='High Temperature (<sup>O</sup>C)')
        trace2 = go.Scatter(x=x, y=lt, marker={'color': 'blue', 'size': 10},  name='Low Temperature (<sup>O</sup>C)')
        data = [trace1, trace2]
        layout=dict(
            xaxis=dict(
                title='Date',
                type='date'),
            yaxis=dict(title='Temperature <sup>o</sup>C')
        )
        figure=dict(data=data,layout=layout)
        div = opy.plot(figure, auto_open=False, output_type='div')
        context['graph'] = div
        return context

class FarmerAdmin(admin.ModelAdmin):
    list_display = [ 'firstName', 'district', 'state']
    search_fields = ['firstName', 'village', 'district', 'state']
    fieldsets = [
        (None, {'fields': ['firstName', 'phone_numbers', 'village', 'district', 'state']}),
        ('Attached Plots',{'fields':['all_plots']})]
    readonly_fields = search_fields + ['phone_numbers', 'all_plots']

    def has_add_permission(self, request, obj = None):
        return False

    def get_search_results(self, request, queryset, search_term):
        """
        modify search to also look for farmers with search_term as phone_no in CRM
        """
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        try:
            phone_no = int(search_term)
        except ValueError:
            pass
        else:
            url ='https://crm.agrostar.in/crmservice/v2/farmerdetailsopen/?searchType=mobile&searchTerm=' + search_term
            response = urllib.request.urlopen(url)
            json_data = json.loads(response.read().decode('utf-8'))
            farmersData = json_data['responseData']['farmers']
            if farmersData:
                try:
                    farmerData = farmersData[0]
                    farmer_id = farmerData['farmerId']
                    queryset |= self.model.objects.filter(cid = farmer_id)
                except:
                    pass
        return queryset, use_distinct

    def phone_numbers(self, obj):
        """
        get all phone numbers of this user from CRM and show them in change view
        """
        try:
            phone_no = ""
            if obj.mobile1:
                phone_no = phone_no + str(obj.mobile1)
            if obj.mobile2:
                phone_no = phone_no + "<br/>" + str(obj.mobile2)
            if obj.mobile3:
                phone_no = phone_no + "<br/>" + str(obj.mobile3)
            return mark_safe(phone_no)
        except:
            pass
    phone_numbers.short_description = "Contact Details"


    def all_plots(self, obj):
        """
        get all plots of this user and display them as link
        """
        plots = Plot.objects.filter(farmer = obj)
        link = ""
        for plot in plots:
            tmp = reverse("admin:app_plot_change", args = [plot.pk,])
            link = link + '<a href="%s">%s</a><br/>'%(tmp, str(plot))
        return mark_safe(link)
    all_plots.short_description = 'Attached plots'



class PlotAdmin(admin.ModelAdmin):
    class Media:
        js = ('https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js',
                    'https://js.api.here.com/v3/3.0/mapsjs-core.js',
                    'https://js.api.here.com/v3/3.0/mapsjs-service.js',
                    'https://js.api.here.com/v3/3.0/mapsjs-ui.js',
                    'https://js.api.here.com/v3/3.0/mapsjs-mapevents.js',)
        css = {
            'all': ('https://js.api.here.com/v3/3.0/mapsjs-ui.css',
                    '/static/css/plot.css',)
        }

    fieldsets = [
        (None, {'fields': ['farmer_link','plot_name','plot_area']}),
        ('JSON',{'fields':['plot_map']}),
        ('Weather', {'fields':['weather_map']}),
        ('Chemical Properties', {'fields':[('pH', 'Boron'),
        ('Organic_Carbon','Nitrogen'),
        ('Phosphorus','Potassium'),
        ('Sodium', 'Calcium'),
        ('Magnesium', 'Sulphur'),
        ('Iron', 'Copper'),
        ('Zinc', 'Manganese')]}),
        ('Physical Properties', {'fields':[('Electrical_Conductivity', 'Cation_Exchange_Capacity'),
        ('Silt', 'Sand'),
        ('Clay','Water_Holding_Capacity'),
        ('Bulk_Density', 'Particle_Density')]})]

    list_display = ['farmer', 'plot_name']
    list_display_related = ['farmer',]
    readonly_fields = ('farmer_link', 'plot_name','plot_area','plot_map','weather_map')
    search_fields = ('farmer__firstName', 'farmer__village', 'farmer__taluka','farmer__district', 'farmer__state', 'farmer__pincode')
    list_filter = ['farmer__state']
    show_full_result_count = False
    date_hierarchy = 'created'

    def has_add_permission(self, request, obj = None):
        """
        disable add option in admin interface
        """
        return False

    def get_queryset(self, request):
        """
        defer coord_json field of plot table for list view
        """
        queryset = super().get_queryset(request)
        if request.resolver_match.func.__name__ == 'changelist_view':
            queryset = queryset.defer("coord_json")
        return queryset

    def get_default_filters(self, request):
        now = timezone.now()
        return {
            'created__year': now.year,
            'created__month': now.month,
        }

    def plot_area(self, obj):
        """
        calculate the area of polygon
        """
        area = 0.0
        d2r = math.pi/180.0
        radius = 6378137.0
        l = []
        coord_json = json.loads(obj.coord_json)
        for point in coord_json['coordinates']:
            l.append([float(point['lat']), float(point['lng'])])
        if len(l)<=2:
            return 0
        else:
            for i in range(len(l)):
                p1 = l[i]
                p2 = l[(i+1)%len(l)]
                area= area+((p2[1]-p1[1])*d2r)*(2+math.sin(p1[0]*d2r)+math.sin(p2[0]*d2r))
            area = abs(area*radius*radius/2.0);
            return mark_safe("%.2f m<sup>2</sup><br>%.2f ft<sup>2</sup><br>%.5f ha<br>%.3f Ac"%(area,area*10.7639,area*0.0001,area*0.000247105))

    def farmer_link(self, obj):
        """
        create a link to farmer foreign key
        """
        link = reverse("admin:app_farmer_change", args = [obj.farmer.pk,])
        return mark_safe('<a href="%s">%s</a>'%(link, obj.farmer))

    def weather_map(self, obj):
        """
        add weather table using iframe in change view
        """
        s = """
            <div id='weather_container' style="width:100%;overflow-x:hidden;overflow-y:hidden;"></div>
            <script>
                point = window.linestring.extractPoint(0);
                $("#weather_container").html("<iframe src='/app/weather?lat="+point.lat+"&lng="+point.lng+ "' width='100%' frameborder='0' onload='resizeIframe(this)'></iframe>");
                function resizeIframe(obj){
                    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
                }
            </script>"""
        return mark_safe(s)

    def plot_map(self, obj):
        """
        add map template in change view
        """
        linestring = "["
        coord_json = json.loads(obj.coord_json)
        for point in coord_json['coordinates']:
            print(point)
            linestring = linestring + str(point['lat']) + ',' + str(point['lng']) + ', 100,'
        last_point = coord_json['coordinates'][0]
        linestring = linestring + str(last_point['lat']) + ',' + str(last_point['lng']) + ', 100'
        linestring = linestring + ']'
        map_html_js = html_template_for_plot_map%(linestring,)
        return mark_safe(map_html_js)

    plot_area.short_description = 'Plot Area'
    weather_map.short_description = 'Weather Forecast'
    plot_map.short_description = 'Plot Map'
    farmer_link.short_description = 'Farmer'


admin.site.register(Farmer, FarmerAdmin)
admin.site.register(Plot, PlotAdmin)
