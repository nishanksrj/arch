from django.urls import include, path
from .views import *

urlpatterns = [
    path('', entry, name = 'entry'),
    path('info/', show_info, name='show_info'),
    path('locate/', locate, name='locate'),
    path('test/', test, name="test")
]
