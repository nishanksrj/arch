from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q
import json, requests, urllib
from app.models import *
from .forms import *


@login_required
def entry(request):
    """
    Render an Html Page

    Args: HttpRequest

    Returns: HttpResponse
    """
    return render(request, 'entry.html')

@login_required
def show_info(request):
    """
    get the farmer's details from CRM and render them along with map

    Args: HttpRequest

    Returns: HttpResponse
    """
    form = CustomerInfoForm(request.POST or None)
    if request.method=="POST" and form.is_valid():
        # if method is POST and form is valid
        form = form.cleaned_data
        phone_no = form.get('phone_no')
        farmer = {}
        try:
            # extract farmer's phone_no and search for the farmer in CRM Database
            url = "https://crm.agrostar.in/crmservice/v2/farmerdetailsopen/?searchType=mobile&searchTerm=" + phone_no
            response = urllib.request.urlopen(url)
            json_data = json.loads(response.read().decode('utf-8'))
            farmerData = json_data['responseData']['farmers']
            if farmerData:
                # if farmer exists in CRM then extract parameters from json
                farmerData = farmerData[0]
                address = farmerData['profileAddress']
                farmer['name'] = farmerData['firstName'] + ' ' + farmerData['middleName'] + ' '  + farmerData['lastName']
                farmer['address'] = address['village'] + ', ' + address['district'] + ', ' + address['state']
                farmer['phone_no'] = phone_no
                farmer['plots'] = Plot.objects.filter(farmer__mobile1 = farmerData['mobile1'])
            else:
                messages.error(request, "User does not exist in our records.")
                return render(request,'entry.html')
        except Exception as e:
            messages.error(request, "Some error occurred. Please contact administration.")
            return render(request,'entry.html')
        return render(request,'info.html', farmer)
    return redirect('/tag')

@login_required
def locate(request):
    """
    get the plot's name and coordinates and store them in db (also create user if he/she doesn't exist)

    Args: HttpRequest

    Returns: HttpResponse
    """
    if request.method == "POST":
        form = GeoDataForm(request.POST)
        if form.is_valid():
            geo_data = form.cleaned_data
            phone_no=geo_data.get('phone_no')
            plot_name=geo_data.get('plot_name')
            try:
                try:
                    # check if farmer exists in System Database
                    farmer = Farmer.objects.get(Q(mobile1=phone_no)|Q(mobile2=phone_no)|Q(mobile3=phone_no))
                except ObjectDoesNotExist:
                    # if not then get it from CRM and add in our system
                    url = "https://crm.agrostar.in/crmservice/v2/farmerdetailsopen/?searchType=mobile&searchTerm=" + phone_no
                    response = urllib.request.urlopen(url)
                    json_data = json.loads(response.read().decode('utf-8'))
                    farmerData = json_data['responseData']['farmers'][0]
                    address = farmerData['profileAddress']
                    farmer = Farmer(cid = farmerData['farmerId'])
                    farmer.mobile1 = farmerData['mobile1']
                    farmer.mobile2 = farmerData['mobile2']
                    farmer.mobile3 = farmerData['mobile3']
                    farmer.firstName = farmerData['firstName']
                    farmer.middleName = farmerData['middleName']
                    farmer.lastName = farmerData['lastName']
                    farmer.village = address['village']
                    farmer.taluka = address['taluka']
                    farmer.district = address['district']
                    farmer.state = address['state']
                    farmer.pincode = address['pinCode']
                    farmer.save(force_insert=True)
            except KeyError as e:
                # if user is not present in CRM record, detect it using KeyError exception and set following error message
                messages.error(request,"User does not exist in our records.")
                # redirect to first page
                return HttpResponseRedirect('/tag')
            except Exception as e:
                # if unknown error occurred then set the following error message
                messages.error(request, "Some error occurred. Please contact administration.")
            try:
                # convert geo_data to geo_json
                c_data = json.loads(geo_data.get('coord'))
                geo_json = json.loads('{"name":"' +  plot_name + '", "geo_json": ' + json.dumps(c_data['geo_json']) + '}')
                # add headers
                headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
                # send POST request to Agromonitoring API to create an extended rectangle to observe NDVI data
                r = requests.post('http://api.agromonitoring.com/agro/1.0/polygons?appid=f64d048d626bdee2e7b518e54f4f4a96', headers = headers, data=json.dumps(geo_json))
                if r.status_code==201:
                    # if success
                    js = r.json()
                    plot = Plot.objects.create(farmer = farmer, geo_id = js['id'], coord_json = json.dumps(c_data['coord_json']),plot_name = plot_name)
                    messages.success(request, 'Data submitted successfully.')
                else:
                    messages.error(request, 'Some error occurred.')
            except Exception as e:
                # exception occurred possibly due to invalid data
                messages.error(request, "Invalid information")
        else:
            # exception occurred possibly due to invalid data
            messages.error(request, "Invalid information")
        # redirect to first page
        return HttpResponseRedirect('/tag')
    else:
        # if GET request then display the first page
        return render(request, 'entry.html', {'form':form})
