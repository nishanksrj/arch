from django import forms
from django.core.validators import RegexValidator

class CustomerInfoForm(forms.Form):
    phone_regex = RegexValidator(regex=r'^\d{10}$', message="Phone number must be of 10 digits.")
    phone_no = forms.CharField(validators = [phone_regex],min_length=10, max_length = 10)

class GeoDataForm(forms.Form):
    phone_regex = RegexValidator(regex=r'^\d{10}$', message="Phone number must be of 10 digits.")
    phone_no = forms.CharField(validators = [phone_regex],min_length=10, max_length = 10)
    plot_name = forms.CharField(max_length=20)
    coord = forms.CharField(max_length = 1024)
